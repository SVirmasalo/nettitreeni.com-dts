<?php
/**
 * The template for displaying 404
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dobby-the-storekeeper
 */

?>

<?php get_header(); ?>

<main class="site-main">

	<?php get_template_part('template-parts/content','none');?>

	<?php get_template_part('template-parts/partial-section','treenivinkit');?>
</main> <!-- .site-main -->

<?php get_footer(); ?>
