<?php
/**
 * The template for displaying all posts by author
 *
 * This is the template that displays all the posts by default.
 * Please note that this is the WordPress construct of archive
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dobby-the-storekeeper
 */

?>
<?php get_header(); ?>

<main class="site-main archive-main archive-default">
	<div class="container">
		<?php 
			if(have_posts()){
				echo '<ul class="post-list">';
				while(have_posts()): the_post();
					echo '<li class="post-list--item">';
						get_template_part('template-parts/content','single');
					echo '</li>';
				endwhile;
				echo '</ul>';
				?>
				<div class="post-navigation">
					<div class="nav-previous alignleft"><?php next_posts_link( __('Vanhemmat','dobbyts') ); ?></div>
					<div class="nav-next alignright"><?php previous_posts_link( __('Uudemmat','dobbyts') ); ?></div>
				</div>

				<?php

			}else{
				get_template_part('template-parts/content','none');
			}
		?>
	</div>
	<?php get_template_part('template-parts/partial-section','treenivinkit');?>
</main>

<?php get_footer(); ?>