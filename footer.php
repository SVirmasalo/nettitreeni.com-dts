<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #page div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dobby-the-storekeeper
 */
?>
	<footer class="site-footer">
		<div class="flex-wrap">
			<div class="footer-col footer-col-1">
				<!-- YHTEYSTIEDOT -->
				<h2><?php _e('Yhteystiedot','dobbyts');?></h2>
				<ul class="contact-information">
					<?php if (have_rows('contact_list','option')){
						while(have_rows('contact_list','option')): the_row();
					?>
						<li><?php the_sub_field('tietue');?></li>
					<?php
						endwhile;
					}else{ ?>
					<li>Nettitreeni.com / Los Blancos</li>
					<li>+358 400 367 876</li>
					<li>info@nettitreeni.com</li>
					<?php } ?>

				</ul>
				<ul class="certs">

					<?php 
						if(have_rows('certs_list','option')){
							while(have_rows('certs_list','option')):the_row(); 
								?>
								<li class="cert">
									<img role="presentation" src="<?php the_sub_field('kuva'); ?>" >
								</li>
								<?php
							endwhile;
						}else{

					?>
						<li class="cert">
							<img role="presentation" src="<?php echo get_stylesheet_directory_uri().'/images/avainlippu.png';?>">
						</li>
					<?php 

						} 

					?>
				</ul>
				<p class="site-meta screen-reader-text"><?php echo date('Y').' - by <a rel="nofollow" href="https://svirmasalo.fi"> Tmi S.Virmasalo </a>' ;?></p>	
			</div>
			<div class="footer-col footer-col-2">
				<!-- SOME -->
				<h2 class="xsmall"><?php the_field('otsikko_foot_right','option'); ?></h2>
				<p class="xl">
					<?php if( get_field('tel_link_bool','option') ) { ?>
						<?php 
							$tel = get_field('tel_foot_right','option');
							$tel_href = "callto:". str_replace(' ', '', $tel);
						?>
						<a  href="<?php echo $tel_href; ?>"><?php echo $tel; ?></a>
					<?php }else{ ?>
						<?php the_field('tel_foot_right','option');?>
					<?php } ?>
				</p>
				<div id="somelinks">
					<?php if(have_rows('some-linkit','option')) : while (have_rows('some-linkit','option')) :the_row(); ?>
							<a href="<?php the_sub_field('linkki'); ?>" rel="me" title="<?php the_sub_field('kanava'); ?>"><i class="round fa <?php the_sub_field('ikoni');?>"></i></a>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</footer> <!-- .site-footer -->
</div> <!-- #Page -->
<?php wp_footer();?>
<script type="text/javascript">
	console.log('Site design and code by Sampo Virmasalo @ https://svirmasalo.fi');
</script>
</body>
</html>