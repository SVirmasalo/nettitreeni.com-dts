<?php
/**
 * The current version of the theme.
 *
 * @package dobby-the-storekeeper
 */
define( 'DOBBYTS_VERSION', '1.0.0' );

// Hide admin
show_admin_bar(false);

/**
 *  Set Yoast SEO plugin metabox priority to low
 */
function dobby_lowpriority_yoastseo() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'dobby_lowpriority_yoastseo' );


/**
* Navigation
*/
function dobbyts_navigation() {
  register_nav_menu('main-menu',__( 'Main Menu', 'dobbyts'));
}
add_action( 'init', 'dobbyts_navigation' );


/**
 * Enqueue scripts and styles.
 */
function dobbyts_scripts() {

	// Fonts
	wp_enqueue_style( 'gfonts', "https://fonts.googleapis.com/css?family=Patua+One", array() );
	wp_enqueue_style( 'fontawesome', "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", array() );

	// Styles.
	wp_enqueue_style( 'theme-styles', get_template_directory_uri()."/css/global.min.css", array() );
	wp_enqueue_style( 'vendor-styles', get_template_directory_uri()."/css/vendor.min.css", array() );
	// Scripts.
	wp_enqueue_script( 'jquery-core' );
	
	// Vendors
	wp_enqueue_script( 'vendor-scripts', get_template_directory_uri().'/js/vendor.min.js', array('jquery'),'0.0', true );

	wp_enqueue_script( 'theme-scripts', get_template_directory_uri().'/js/all.min.js' , array(),'0.0', true );

}
add_action( 'wp_enqueue_scripts', 'dobbyts_scripts' );

/**
* Theme extensions --> (./lib/dts-theme-extensions.php)
*/
include_once(__DIR__.'/lib/dts-theme-extensions.php');

/**
* WooCommerce --> (./lib/dts-woocommerce-rules.php)
*/
include_once(__DIR__.'/lib/dts-woocommerce-rules.php');


/**
* Some Dobby helpers
*/

/**
* Make excerpt
*/
function make_excerpt($phrase, $max_words) {
   $phrase_array = explode(' ',$phrase);
   if(count($phrase_array) > $max_words && $max_words > 0)
      $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
      $phrase = strip_tags($phrase);
   return $phrase;
}

// Loop posts like products
// Call with [dobby_posts cat="category_id" limit="max-cols" carousel="bool" vertical="vertical/horizontal"]
function proudct_like_posts($atts){
	
	$args = array(
		'posts_per_page' => ($atts["limit"] ? intval($atts["limit"]) : 5),
		'offset' => 0,
		'category' => intval($atts["cat"]),
		'post_status' => 'publish'
	);
	$postsArray = get_posts($args);

	if($atts["carousel"] === "true"){
		$carousel = "carousel";
		$dataType = "data-vertical=".$atts["vertical"];
	}else{
		$carousel = "";
		$dataType = "";
	}
	
	$postList_html = "<ul class='dobby-post-loop ".$carousel."' ".$dataType." >";

	foreach ($postsArray as $post) {

		/**
		* Model:
		* <li class="post-id dobby-post type-post">
		* 	<a class="dobby-loop-post" href="post-link">
		*		<img class="dobby-loop-post__image wp-post-image" />
		*		<h2 class="dobby-loop-post__title">Title</h2>
		*		<p class="dobby-loop-post__readmore"> Read more </p>
		*	</a>
		* </li>
		*/

		$post_id = $post->ID;
		//$post_excerpt = (get_the_excerpt($post_id) ? get_the_excerpt($post_id) : wp_trim_excerpt("") );

		$post_excerpt = $post->post_excerpt;
		if($post_excerpt == ''){
			$post_excerpt = make_excerpt($post->post_content, 25);
		}

		// Form the object
		$postList_html .= 
			"<li class='post-".$post_id." dobby-post'>
				<a class='dobby-loop-post__link' href='".get_the_permalink($post_id)."'>
					<h3 class='dobby-loop-post__title'>".$post->post_title."</h3>
					<p class='dobby-loop-post__excerpt'>".$post_excerpt."</p>
					<p class='dobby-loop-post__readmore'>".__('Lue lisää','dobby')."</p>
				</a>
			</li>";
	}

	wp_reset_postdata();

	$postList_html .= "</ul>";

	return $postList_html;
}
add_shortcode('dobby_posts', 'proudct_like_posts');

// Make background image style-tag from url 
function bgimg($url){
	return ' style=background-image:url('.$url.'); ';
}
