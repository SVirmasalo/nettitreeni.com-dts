<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <header class="site-header">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dopbby-the-storekeeper
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri();?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri();?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri();?>/images/favicon/manifest.json">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri();?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/favicon/favicon.ico">
	<meta name="msapplication-config" content="<?php echo get_template_directory_uri();?>/images/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<?php $google = get_field('google','option'); ?>
	<?php if($google['google_analytics_enabled'] ): ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google['seurantatunnus']; ?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', <?php echo "'".$google["seurantatunnus"]."'"; ?>);
		</script>
	<?php endif; ?>


	<?php wp_head(); ?>
</head>
<body <?php body_class();?> >
	<!-- SITE-HEADER -->
	<header class="site-header">
		<div class="pre-nav-box">
			<div class="logo-col">
				<?php /*data-pngSrc for fallbacks*/?>

				<a style="display: block;line-height: 1;width: 100%; margin:0;" class="no-style-link" href="<?php echo get_home_url(); ?>">
				<img role="presentation" id="logo" data-pngSrc="<?php echo get_stylesheet_directory_uri().'/images/nettitreeni-logo-white.png'; ?>" src="<?php echo get_stylesheet_directory_uri().'/images/nettitreeni-logo-white.svg'; ?>">
				</a>
				<p class="logo-title"><?php _e('Suomen Henkilökohtaisin Treenisivusto');?></p>
			</div>
			<div class="nav-toggle-col">
				<button id="nav-open" class="nav-toggle">
					<i class="fa fa-align-right"></i>
				</button>
			</div>
		</div>
		<div id="nav-box" class="" aria-hidden="true">
			<div class="nav-box--controls">
				<button id="nav-close" class="nav-toggle">
					<i class="fa fa-close"></i>
				</button>
				<label for="site-nav"><?php _e('Valikko','dobbyts');?></label>
			</div>	
			<?php 
				$navArgs = array(
					'container' => 'nav',
					'container_id' => 'site-nav',
					'dept' => 2,
				);
				wp_nav_menu($navArgs);
			?>
			
			<script type="text/javascript">
				jQuery(document).ready(function(){

					function calcSubmenu(){
						var childHeight = this.lastElementChild.firstElementChild.clientHeight;
						var childCount = this.lastElementChild.children.length;
						this.dataset.height = childCount * childHeight;
						this.dataset.steps = childCount;
					}

					function toggleSubMenu(menuItem){
						this.addEventListener('click',function(e){
							e.stopPropagation();
							jQuery(this).toggleClass('active');
							jQuery(menuItem).toggleClass('show-sub-menu');
							if(jQuery(menuItem).hasClass('show-sub-menu')){
								//Open menu
								var targetHeight = menuItem.dataset.height;
								jQuery(menuItem.lastElementChild).animate({
									height:targetHeight
								},300);
							}else{
								//Close menu
								jQuery(menuItem.lastElementChild).animate({
									height:0
								},300);
							}
						});
					}

					jQuery('.menu-item-has-children').each(function(k,v){
						var thisItem = v;
						try{
							calcSubmenu.call(thisItem);
						}catch(err){
							console.log('not able to measure height of selected list-item: ', err );
						}
						var thisCaret = document.createElement('button');
						jQuery(thisCaret).addClass('sub-menu-toggler').append('<i class="fa fa-caret-left"></i>');
						toggleSubMenu.call(thisCaret, thisItem);
						try{
							jQuery(thisItem).prepend(thisCaret);
						}catch(err){
							console.log('Mobile navigation caused this error: ', err);
						}
					});
				});
			</script>
			<div class="nav-box--note">
				<label for="call-us"><?php _e('Soita','dobbyts'); ?>:</label>
				<a id="call-us" href="#callto:+358400367876">+358 400 367 876</a>
			</div>
		</div>
	</header>
	<!-- !SITE-HEADER -->
	<div id="page" class="site">
		<!-- HERO -->
		<?php if(is_front_page() ) { ?>
			<?php get_template_part('template-parts/partial-hero','full'); ?>
		<?php } else{ ?>
			<?php get_template_part('template-parts/partial-hero','page'); ?>
		<?php } ?>
		<!-- !HERO -->

		