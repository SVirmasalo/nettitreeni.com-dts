'use strict';

(function($) {

	function initMobileNavigation(){
		var navOpener = document.getElementById('nav-open');
		var navCloser = document.getElementById('nav-close');
		var navBox = document.getElementById('nav-box');

		function animate_snavOpen(){
			console.log('open:', this);
			$(this).animate({
				'width':'300px',
			},300);
		}
		function animate_snavClose(){
			console.log('close:', this);
			$(this).animate({
				'width':'0px',
			},300);
		}

		$(navOpener).on('click',function(e){
			e.stopPropagation();
			$(navBox).toggleClass('open');
			navCloser.disabled = false;
		});

		$(navCloser).on('click',function(e){
			e.stopPropagation();
			$(navBox).toggleClass('open');
			navCloser.disabled = true;
		})
	}

	function equalizeHeights(listOfObjects){
		/**
		* Equalizes the height of its children
		*/
		try{
			var eachSel = $(this.selector+'  > *');
			var maxH = 0;
			eachSel.each(function(k,v){
				if( $(v).height() > maxH ){
					maxH = $(v).height();
				}
			});
			eachSel.height(maxH);

		}catch(err){
			console.log('equalizeHeights caused an error: ', err);
		}
	}

	function initSlick(options, callback){
		/**
		* If options are not specifically sent, slick will be using thes defaults
		* note: on default viewport size, there will be as many slides as there's set on attribute data-cards
		*/ 
		var defaultOpts = {
			accessibility:true,
			infinite:false,
			dots:true,
			arrows:false,
			slidesToShow: (this.attr("data-cards") ? this.attr("data-cards") : 4) ,
			autoplay:false,
			autoplaySpeed:3500,
			adaptiveHeight:false,
			responsive: [
				{
					breakpoint: 768,
					settings:{
						slidesToShow:2,
					}
				},
				{
					breakpoint: 480,
					settings:{
						slidesToShow:1,
						centered:true
					}
				}
			]
		}
		if(options){
			for(var option in options){

				if(options.hasOwnProperty(option)){
					defaultOpts[option] = options[option]
				};
			}
		}
		
		options = defaultOpts;

		

		/**
		* Callback
		* If there's some callback, use it here.
		*/
		if(callback) callback.call(this);

		this.slick(options);
	}


	$(document).ready(function(){

		if(! $('body').hasClass('home') ){
			var headerBlock = $('#hero-page .hero-container');
			var heroBlock = $('#hero-page');

			heroBlock.css('margin-bottom', ( headerBlock.height() / 2 ) + 32  + 'px');
			
		}

		initMobileNavigation();
		initSlick.call($('#fp-facts .container'), {centered:true,accessibility:true,adaptiveHeight:false}, equalizeHeights);
		initSlick.call($('#services .product-row-wrapper .products'), {adaptiveHeight:true,slidesToShow:3,dots:false, arrows:true});
		initSlick.call($('#references .ref-row-wrapper .refs'),{
			autoplay:true,
			autoplaySpeed: 3000,
			responsive:[
				{
					breakpoint:990,
					settings:{
						slidesToShow:2
					}
				},
				{
					breakpoint:480,
					settings:{
						slidesToShow:1
					}
				}
			]
		});

		initSlick.call($('.alternative-upsells ul.products'),{adaptiveHeight:true});


		/**
		* GALLERY
		*/
		try{

			/**
			* Modify MPP options
			*/
			 $.extend(true, $.magnificPopup.defaults, {
			 	gallery:{
			 		tCounter:'%curr% / %total%',
			 	},
			 	image: {
			 		tError: '<a href="%url%">Kuvaa</a> ei voitu avata.'
			 	}
			 });

			$('.examples_gallery').magnificPopup({
				delegate: 'a',
				type: 'image',
				closeOnContentClick: false,
				closeBtnInside: false,
				mainClass: 'mfp-with-zoom mfp-img-mobile',
				image: {
					verticalFit: true,
					/*titleSrc: function(item) {
						return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
					}*/
				},
				gallery: {
					enabled: true
				},
				zoom: {
					enabled: true,
					duration: 300, // don't foget to change the duration also in CSS
					opener: function(element) {
						return element.find('img');
					}
				}
			});
		}catch(err){
			console.log('gallery caused an error: ', err);
		}


		/**
		* LOGIN POPUP
		*/
		var login_popup = $('.popup-with-form');
		try{
			login_popup.magnificPopup({
				type: 'inline',
				preloader: false,
				focus: '#username',

				// When elemened is focused, some mobile browsers in some cases zoom in
				// It looks not nice, so we disable it:
				callbacks: {
					beforeOpen: function() {
						if($(window).width() < 700) {
							this.st.focus = false;
						} else {
							this.st.focus = '#username';
						}
					}
				}
			});
		}catch(err){
			console.log('Login popup caused this error: ', err);
		}

	}); // Document ready

})( jQuery );
