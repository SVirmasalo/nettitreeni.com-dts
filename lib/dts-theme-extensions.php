<?php 
/**
* Description: Modifications to theme that I've found usefull.
*
* @package dobby-the-storekeeper
* @version 0.4.0
*
*/

if ( ! defined( 'ABSPATH' ) ) exit;
/**
* REGISTER SIDEBAR WIDGET
*/ 
function dobbyts_default_sidebar_page() {

  register_sidebar( array(
    'name'          => __('Sivun sivuvalikko', 'dobbyts'),
    'id'            => 'page_1',
    'before_widget' => '<div class="sidebar-container">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="sidebar-header">',
    'after_title'   => '</h3>',
  ) );

}
add_action( 'widgets_init', 'dobbyts_default_sidebar_page' );

function dobbyts_default_sidebar_blog() {

  register_sidebar( array(
    'name'          => __('Arkiston ja artikkelin sivuvalikko', 'dobbyts'),
    'id'            => 'post_1',
    'before_widget' => '<div class="sidebar-container">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="sidebar-header">',
    'after_title'   => '</h3>',
  ) );

}
add_action( 'widgets_init', 'dobbyts_default_sidebar_blog' );


/**
* Options for ACF
*/
$optionsArgs_top = array(
  'page_title' => 'Teeman lisäasetukset',
  'menu_title' => 'Teeman lisäasetukset',
  'menu_slug' => 'teeman-asetukset',
  'redirect' => false,
);
$optionsArgs_footer = array(
  'page_title' => 'Teeman alaosan asetukset',
  'menu_title' => 'Teeman alaosan asetukset',
  'menu_slug' => 'teeman-alaosan-asetukset',
  'parent_slug' => 'teeman-asetukset',
  'redirect' => true,
);

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page( $optionsArgs_top );
  acf_add_options_page( $optionsArgs_footer );
}
