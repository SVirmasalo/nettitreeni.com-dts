<?php
/**
 * The sidebar containing the main widget area
 * Implement your custom sidebar to this file.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dobby-the-storekeeper
 */
?>

<aside id="widget-area">
	<?php if(is_woocommerce() ): ?>
		<!-- 
		<button for="widgets-dropown" id="toggle-widgets" class="visible-tab">
			<label class="screen-reader-text" for="toggle-widgets"><?php _e('Kauppavalikko','dobbyts'); ?></label>
			<i class="fa fa-plus round"></i>
		</button>
		<div name="widgets-dropown" class="widgets-wrap">
			<div class="wc-widgets card">
				<?php if ( is_active_sidebar( 'woo_left_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'woo_left_1' ); ?>
					</div><!-- #primary-sidebar -->
					<!-- 
				<?php endif; ?>
			</div>
			<div class="wc-widgets card">
				<div id="secondary-sidebar" class="secondary-sidebar widget-area">
					<div class="sidebar-container">
					<h3 class="sidebar-header screen-reader-text">
						<?php _e('Tili', 'dobbyts');?>
					</h3>
					<ul class="uac-controls">
						<li>
							<?php dobbyts_get_uacs(); //User account controls ?>
						</li>
						<li>
							<a 	class="cart-customlocation" 
							href="<?php echo wc_get_cart_url(); ?>" 
							title="<?php _e( 'View your shopping cart' ); ?>">
							<?php echo sprintf ( _n( '%d '.__('item'), '%d '.__('items'), WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>&nbsp;/&nbsp;<?php echo WC()->cart->get_cart_total(); ?>		
							</a>
						</li>
					</ul>
					<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 * @hooked WC_Structured_Data::generate_website_data() - 30
						 */
						remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);
						add_action('woocommerce_before_main_content','woocommerce_catalog_ordering',30 );
						do_action( 'woocommerce_before_main_content' );
					?>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			/**
			* Toggle options
			*/
			var button_uac = jQuery('#toggle-widgets');
			var sidebar_wrapper = jQuery('div[name="widgets-dropown"]');
			jQuery(document).ready(function(){
				button_uac.on('click',function(e){
					e.stopPropagation();
					jQuery(this).toggleClass('active');
					sidebar_wrapper.toggle('fast','linear');
				});
			});
		</script>

		-->
	<?php endif; ?>
	<?php if( is_page() ): ?>
		<?php if ( is_active_sidebar( 'page_1' ) ) { ?>

			<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
				<div class="sidebar-container">
					<div class="widgets-wrap">
						<?php dynamic_sidebar( 'page_1' ); ?>
					</div>
				</div>
			</div><!-- #primary-sidebar -->

		<?php }else{ ?>
			<div id="primary-sidebar" class="primary-sidebar recent-posts" role="complementary">
				<div class="sidebar-container recent-posts">
					<h3 class="sidebar-header">
						<?php _e('Uudet julkaisut', 'dobbyts');?>
					</h3>
					<div class="widgets-wrap">
						<?php echo do_shortcode('[dobby_posts cat=13 limit=2 carousel=true vertical=true]',false); ?>
					</div>
				</div>
			</div><!-- #primary-sidebar -->
		<?php } ?>

	<?php endif; ?>		
	<?php if( is_single() && !is_woocommerce() ): //Only for blogs, not for single products ?> 
		<?php //if ( is_active_sidebar( 'post_1' ) ) : ?>
			<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
				<div class="widgets-wrap">
					<h3 class="sidebar-header">
						<?php _e('Uudet julkaisut', 'dobbyts');?>
					</h3>
					<div class="widgets-wrap">
						<?php echo do_shortcode('[dobby_posts cat=13 limit=2 carousel=false vertical=false]',false); ?>
					</div>
					<?php dynamic_sidebar( 'post_1' ); ?>
				</div>
			</div><!-- #primary-sidebar -->
		<?php // endif; ?>
	<?php endif; ?>		
</aside> <!-- #widget-area -->