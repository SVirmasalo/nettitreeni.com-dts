<?php
/**
 * The template for displaying single post
 *
 * This is the template that displays all posts by default.
 * Please note that this is the WordPress construct of post
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dobby-the-storekeeper
 */

?>

<?php get_header(); ?>

<main class="site-main">
	<?php 
		if(have_posts()){
			while(have_posts()): the_post();
				get_template_part('template-parts/content','single');
			endwhile;
		}else{
			get_template_part('template-parts/content','none');
		}
	?>
	<?php get_template_part('template-parts/partial-section','treenivinkit');?>
</main> <!-- .site-main -->

<?php get_footer(); ?>
