<?php
	if(have_rows('kortit')):
		while(have_rows('kortit')): the_row();
			$card_1 = get_sub_field('kortti_vasen');
			$card_2 = get_sub_field('kortti_oikea');
		endwhile;
	endif;
?>
<section id="fp-facts" class="cards-wrapper">
	<div class="container" data-cards="2">
		<article class="card">
			<i class="fa <?php echo $card_1["kortin_ikoni"]; ?> pin" role="presentation"></i>
			<header class="card-header">
				<h2 class="small">
					<?php 
						echo $card_1["kortin_otsikko"]; 
					?>
				</h2>
			</header>
			<div class="article-body">
				<?php 
					echo $card_1["kortin_sisalto"];
				?>
			</div>
		</article>
		<article class="card">
			<i class="fa <?php echo $card_2["kortin_ikoni"]; ?> pin" role="presentation"></i>
			<header class="card-header">
				<h2 class="small">
					<?php 
						echo $card_2["kortin_otsikko"]; 
					?>
				</h2>
			</header>
			<div class="article-body">
				<?php 
					echo $card_2["kortin_sisalto"];
				?>
			</div>
		</article>
	</div>
	<script type="text/javascript">
		jQuery('#fp-facts article .article-body ul').each(function(k,v){
			jQuery(this).addClass('checkmarks');
			jQuery(this).children().prepend('<i class="fa fa-check round positive"></i>&nbsp;');
		});
	</script>
</section>
<section id="services">
	<div class="container">
		<div class="content-wrap">
			<div class="col col-1">
				<header class="section-header legend">
					<h2>Palvelumme</h2>
				</header>
				<div class="product-row-wrapper">
					<?php echo do_shortcode('[recent_products limit="6"]',false);?>
				</div>
				<footer class="section-footer">
					<a class="btn positive" href="<?php the_permalink(wc_get_page_id('shop')); ?> "><?php _e('Verkkokauppaan', 'dobbyts'); ?></a>
				</footer>
			</div>
			<div class="col col-2 hidden-tab">
				<header class="section-header legend">
					<h2><?php _e('Blogi','dobbyts');?></h2>
				</header>
				<div class="product-row-wrapper vertical-carousel">
					<?php 
						/**
						* Feed settings
						*/
						$feedSettings = get_field('uutis-syote');
						$feedCat = $feedSettings["haluttu_syote"];
						$feedLimit = $feedSettings["naytettavien_tekstien_maara"];

					?>
					<?php echo do_shortcode('[dobby_posts cat="'.$feedCat.'" limit='.$feedLimit.' carousel=true vertical=true]',false);?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if(have_rows('referenssit')): ?>
	<?php 
		if(have_rows('referenssit_headings')): while (have_rows('referenssit_headings')):the_row(); 
			# code...
			$refs_heading = get_sub_field('refsh_heading');
			$refs_subheading = get_sub_field('refsh_subheading');
			$refs_redirect = get_permalink(get_sub_field('refsh_redirect'));
		endwhile; endif;
	?>
	<section id="references" class="cloud">
		<div class="container">
			<header class="section-header">
				<h2><a class="no-style-link" href="<?php echo $refs_redirect;?>"><?php echo $refs_heading;?></a></h2>
				<p class="section-header--subtitle"><?php echo $refs_subheading; ?></p>
				<p class="section-header--icon"><i class="fa fa-check"></i></p>
			</header>
			<div class="ref-row-wrapper">
				<ul class="refs carousel">
					<?php 
						while(have_rows('referenssit')): the_row(); 
							$link = (get_sub_field('linkki') ? get_sub_field('linkki') : '#');
							$kuva = get_sub_field('logo_yritys');
							?>	
								<li class="ref-item">
									<a href="<?php echo get_permalink($link); ?>">
										<img src="<?php echo $kuva["sizes"]["medium"];  ?> " alt="<?php the_sub_field('nimi'); ?>">
									</a>
								</li>
							<?php
						endwhile;
					?>
				</ul>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php get_template_part('template-parts/partial-section','treenivinkit');?>