<div class="container">
	<div class="page-content-wrap">
		<div class="card article-wrap">
			<article <?php post_class(); ?>>
				<h2><?php _e('Hakemaasi sivua ei valitettavasti löytynyt','dobbyts');?></h2>
				<p>
					<?php _e('Klikkaamasi linkki saattoi olla vanhentunut tai virheellisesti kirjoitettu. Ei se mitään. Kokeile toista osoitetta, tai mene ','dobbyts'); ?>
					<a href="<?php echo get_home_url(); ?>"><?php _e('etusivulle','dobbyts');?>.</a>
				</p>
			</article>
		</div>
	</div>
</div>
<?php if(! is_woocommerce() ):?>
	<div class="container alternative-upsells">
		<header class="legend">
			<h2><?php _e('Palvelumme','dobbyts');?></h2>
		</header>
		<?php echo do_shortcode('[recent_products limit="6"]',false);?>
	</div>
<?php endif; ?> 