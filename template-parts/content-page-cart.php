<?php
/**
 * Cart Page Content
 *
 * This template can be overridden by copying it to yourtheme/template-parts/content-page-cart.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dobby-the-storekeeper
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="container">
	<div class="page-content-wrap">
		<div class="card article-wrap">
			<article <?php post_class(); ?>>
				<?php the_content(); ?>
			</article>
		</div>
	</div>
</div>

<?php if(! is_woocommerce() ):?>
	<div class="container alternative-upsells">
		<header class="legend">
			<h2><?php _e('Palvelumme','dobbyts');?></h2>
		</header>
		<?php echo do_shortcode('[recent_products limit="6"]',false);?>
	</div>
<?php endif; ?> 