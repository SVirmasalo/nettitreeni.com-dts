<div class="container">
	<div class="page-content-wrap">
		<div class="card article-wrap">
			<article <?php post_class(); ?>>
				<?php the_content(); ?>
			</article>
		</div>
		<div class="card sidebar-wrap">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php if(! is_woocommerce() ):?>
	<div class="container alternative-upsells">
		<header class="legend">
			<h2><?php _e('Palvelumme','dobbyts');?></h2>
		</header>
		<?php echo do_shortcode('[recent_products limit="6"]',false);?>
	</div>
<?php endif; ?> 