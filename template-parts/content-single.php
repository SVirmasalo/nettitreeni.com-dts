<?php if (is_archive()) :?>
	<?php 
		if ( has_post_thumbnail() ) {
			$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
	   	}else{
	   		$feat_image_url = get_template_directory_uri().'/images/nettitreeni-askel-kerrallaan.jpg';	
		}
	?>
	<article <?php post_class('card') ;?> >
		<a class="dobby-loop-post__link" href="<?php the_permalink(get_the_ID()); ?>">
		<img src="<?php echo $feat_image_url; ?>">
		<?php the_title('<h2 class="dobby-loop-post__title">','</h2>'); ?>
		<p class="dobby-loop-post__excerpt"><?php the_excerpt(); ?></p>
		<span class="dobby-loop-post__readmore"><?php _e('Lue lisää','dobbyts');?></span>
		</a>
		<p class="screen-reader-text dobby-loop-post__meta small">
			<span><?php _e('Kirjoittanut: ','dobbyts');?></span>
			<span><a href="<?php the_author_link(); ?>" rel="author"><?php the_author(); ?></a></span>
		</p>
	</article>
<?php endif; ?>

<?php if (is_single()) :?>
	<div class="container">
		<div class="page-content-wrap">
			<div class="card article-wrap">
				<article <?php post_class(); ?>>
					<?php the_content(); ?>
				</article>
			</div>
			<?php if(!is_archive() ) :?>
				<div class="card sidebar-wrap">
					<?php get_sidebar(); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php if(! is_woocommerce() && is_archive() === false ):?>
		<div class="container alternative-upsells">
			<header class="legend">
				<h2><?php _e('Palvelumme','dobbyts');?></h2>
			</header>
			<?php echo do_shortcode('[recent_products limit="6"]',false);?>
		</div>
	<?php endif; ?> 
<?php endif; ?>