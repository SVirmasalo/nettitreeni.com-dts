<?php 
	
	$heroImg = ''; //get_field('hero_full_image');
	if(!$heroImg){
		$heroImg = get_template_directory_uri().'/images/nettitreeni-askel-kerrallaan.jpg';
	}

?>
<div id="hero-full" class="hero" <?php echo bgimg($heroImg);?>>
	<div class="hero-container">
		<p class="punch">
			Askel kerrallaan kohti uutta elämää
		</p>
		<a class="btn positive" href="<?php the_permalink(wc_get_page_id( 'shop' ));?>">Kauppa</a>
	</div>
</div>