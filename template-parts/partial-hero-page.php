<?php
   if ( has_post_thumbnail() ) {
		$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
   }else{
   		$feat_image_url = get_template_directory_uri().'/images/nettitreeni-askel-kerrallaan.jpg';	
   }
?>

<div id="hero-page" class="hero" <?php echo bgimg($feat_image_url);?>>
	<div class="hero-container">
		<?php woocommerce_breadcrumb(); ?>
		<?php if( is_woocommerce() && !is_single() ) { ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
			<?php endif; ?>
		<?php }elseif( is_woocommerce() && is_single() ){ ?>
				<?php the_title('<h1 class="page-title woocommerce-product-header__title">','</h1>'); ?>
		<?php }elseif( is_category() && !is_woocommerce() ){ ?>
			<h1 class="page-title"><?php echo single_cat_title(); ?></h1>
		<?php }else{ ?>
			<?php if(! is_404() ){ ?>
				<?php the_title('<h1 class="page-title">','</h1>');?>
			<?php } else { ?>
				<h1 class="page-title"><?php _e('Sivua ei löytynyt', 'dobbyts'); ?> </h1>
			<?php } ?>
		<?php } ?>


		<?php

			$showShortcuts = get_field('verkkokaupan_asetukset','option');
			$showShortcuts = $showShortcuts['show_shop_shortcuts'];
			if($showShortcuts):
		?>
			<?php if(! is_user_logged_in()){ ?>
				<a href="#login-form-modal" class="shortcuts popup-with-form"><span class="screen-reader-text"><?php _e('Kirjaudu sisään','dobbyts'); ?></span><i class="fa fa-lg fa-sign-in"></i></a>
			<?php }else{ ?>

				<?php if (! is_cart()): ?>
				<a class="shortcuts" href="<?php echo the_permalink(wc_get_page_id( 'myaccount' )); ?>"><span class="screen-reader-text"><?php _e('Oma tili','dobbyts');?></span><i class="fa fa-lg fa-user"></i></a>
				<a class="shortcuts" href="<?php echo wp_logout_url(get_permalink()); ?>"><span class="screen-reader-text"><?php _e('Kirjaudu ulos','dobbyts');?></span><i class="fa fa-lg fa-sign-out"></i></a>
				<?php endif; //is_cart ?>


			<?php } //end if else ?>
			<a class="shortcuts" href="<?php the_permalink(wc_get_page_id( 'cart' ));?>"><span class="screen-reader-text"><?php _e('Ostoskori','dobbyts');?></span><i class="fa fa-lg fa-shopping-cart"></i></a>
		<?php endif; //If showShortcuts ?>
		<a class="shortcuts cart-block" title="<?php _e('Cart','woocommerce');?>" href="<?php the_permalink(wc_get_page_id( 'cart' ));?>"><span class="screen-reader-text"><?php _e('Ostoskori','dobbyts');?></span><i class="fa fa-lg fa-shopping-cart"></i></a>
	</div>
</div>