<?php 
	/**
	* ACF CONTENT VARIABLES
	*/

	$partial_group_tips = get_field('treenivinkit-banneri','option');
	$tips_header = $partial_group_tips["otsikko"];
	$tips_backdrop = $partial_group_tips["taustakuva"];
	$tips_link_label = $partial_group_tips["treenivinkit_teksti"];
	$tips_link_url = get_category_link($partial_group_tips["treenivinkit_ohjaus"]);
	$tips_blog_label = $partial_group_tips["blogilinkin_teksti"];
	$tips_blog_url = get_category_link($partial_group_tips["blogilinkin_categoria"]);

	if(!$tips_backdrop){
		$tips_backdrop = "<img src=".get_stylesheet_directory_uri()."/images/treenivinkit-ammatilaisilta.jpg alt=".$tips_header.">";
	}


?>
<section id="tips" class="big imaged">
	<div class="backdrop">
		<img src="<?php echo $tips_backdrop["url"]; ?>" alt="<?php echo $tips_backdrop["alt"]; ?>" />
		
		<span class="shade" role="presentation"></span>
	</div>
	<div class="section-content">
		<div class="content-wrap">
			<h2 class="hidden-tab"><?php echo $tips_header; ?></h2>
			<a href="<?php echo $tips_link_url; ?>" class="btn small positive">
				<span class="visible-tab"><?php _e('Treenivinkit', 'dobbyts'); ?></span>
				<span class="hidden-tab"><?php echo $tips_link_label; ?></span>	
			</a>
			<a href="<?php echo $tips_blog_url; ?>" class="btn small note visible-tab"><?php echo $tips_blog_label; ?></a>
		</div>
	</div>
</section>